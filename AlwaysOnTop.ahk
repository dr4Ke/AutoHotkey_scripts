﻿#NoEnv  ; Recommended for performance and compatibility with future AutoHotkey releases.
; #Warn  ; Enable warnings to assist with detecting common errors.
SendMode Input  ; Recommended for new scripts due to its superior speed and reliability.
SetWorkingDir %A_ScriptDir%  ; Ensures a consistent starting directory.

;Menu, Tray, Icon, AlwaysOnTop.ico


^SPACE::

WinGet, ExStyle, ExStyle, A
if (ExStyle & 0x8) {
  Winset, Alwaysontop, OFF, A  ; Window not always on top
  ;Winset, Transparent, 255, A
  WinSet, Style, +0xC0000, A  ; Toggle the active window's thin-line border (WS_BORDER).
} Else {
  Winset, Alwaysontop, ON, A  ; Window always on top
  ;Winset, Transparent, 170, A
  WinSet, Style, -0xC0000, A  ; Toggle the active window's thin-line border (WS_BORDER).
}
Return
