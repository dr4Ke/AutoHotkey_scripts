; Switch between windows of the same application with Alt+(key above Tab)
; Icon: made by Freepik (www.freepik.com), licence CC 3.0 BY
;       from https://www.flaticon.com/free-icon/switch-window_71630
; Script Licence: CC0 (Public Domain)
; Source: https://framagit.org/dr4Ke/AutoHotkey_scripts

KeyName := GetKeyName("sc029")
Menu, Tray, Tip, Switch between windows of the same applications with 'Alt+%KeyName%'

*!SC029::
WinGetClass, ActiveClass, A
WinGet, WinClassCount, Count, ahk_class %ActiveClass%
If WinClassCount = 1
    ;TrayTip, Only one window, Exiting
    Return
WinGet, List, List, % "ahk_class " ActiveClass

index := 0
if not GetKeyState("Shift") {
    index := 1
}
;MsgBox, Entering Loop
While GetKeyState("Alt") {
    If GetKeyState("Shift") {
        index := Mod(List + index - 2, List) + 1
    } else {
        index := Mod(List + index, List) + 1
    }
    WinGet, State, MinMax, % "ahk_id " List%index%
    ;Traytip, Inside loop, %List%-%index%-List%index%
    if (State == -1)
    {
        continue
    }
    WinID := List%index%
    WinActivate, % "ahk_id " WinID
    ErrorLevel := 1
    sleep 50
    While (ErrorLevel != 0) and GetKeyState("Alt") {
        KeyWait, sc029, DT1
    }
}
;Traytip, Exit, Exit from script
return
